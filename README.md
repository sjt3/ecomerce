# Ecomerce

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 15.2.6.

## Sobre el proyecto

Corresponde al trabajo final del curso "Angular Avanzado"
dictado en San Juan Tec, Año 2023, Ministerio de la Producción. 

Instructor: José Mateo Rodriguez
Alumno: Dario Javier Rodriguez

San Juan - Argentina