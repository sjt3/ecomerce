import { Category, Product } from "../interfaces";

export const CATEGORY: Category[] = [{"id": 1, "name": "Supermercado",  "description": "Productos de almacen y alimentos"}, 
                                {"id": 2, "name": "Tecnología",  "description": "Productos electrónicos"},
                                {"id": 3, "name": "Muebles y Hogar",  "description": "Muebles y decoración para el hogar"}];

export const PRODUCT: Product[] = [{"id": 1, "title": "Azucar Chango",  "categoryId": 1, mark: "Chango", model: "Azucar", description: "Azucar Refinada", barcode: "15321398465", mainImage: "../assets/img/products/1/main.jpg", price: 450}, 
                                                                                                                                                                
{"id": 2, "title": "Dogui 15kg Perro",  "categoryId": 1, mark: "Purina", model: "Dogui", description: "Alimento para perros sabor carne por 15kg", barcode: "15321321560", mainImage: "../assets/img/products/2/main.png", price: 3900},
{"id": 3, "title": "Samsung A04",  "categoryId": 2, mark: "Samsung", model: "A04 Lite", description: "Celular Basico", barcode: "15321321555", mainImage: "../assets/img/products/3/main.jpg", price: 59800},
{"id": 4, "title": "Mesa de Madera",  "categoryId": 3, mark: "Century", model: "J5Mesa", description: "Madera de pino - Medidas 160cmx70cm", barcode: "153213211112", mainImage: "../assets/img/products/4/main.jpg", price: 35000},
{"id": 5, "title": "Silla de Madera",  "categoryId": 3, mark: "Century", model: "J5Mess", description: "Silla de pino - Medidas 160cmx70cm", barcode: "153213211112", mainImage: "../assets/img/products/5/main.jpg", price: 35000},];