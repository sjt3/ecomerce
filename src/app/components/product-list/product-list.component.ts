import { Component, OnInit } from '@angular/core';
import { Product } from 'src/app/interfaces';
import { Category } from 'src/app/interfaces';
import { PRODUCT, CATEGORY } from 'src/app/data/products';
import { AddcartService } from 'src/app/services/addcart.service';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit{
  products: Product[] | undefined;
  categories: Category[] | undefined; 

  constructor (private addCartService: AddcartService){}

  ngOnInit(): void {
    this.products = PRODUCT;
    this.categories = CATEGORY;
    //throw new Error('Method not implemented.');
  }

  addCartInList(idSelect: number){
    return this.addCartService.addCart(idSelect);
  }

  elimineProdFromListCart(idSelect: number){
    return this.addCartService.elimineProdFromListCart(idSelect);
  }

  productInCart(id: number){
    return this.addCartService.isIdInCart(id);
  }

  classAddingToCart(id: number){
    let valor = this.productInCart(id);
    let classes = "";
    if (valor){
      classes = "btn btn-secondary disabled product-btn btn-sm";
    } else {
      classes = "btn btn-primary product-btn btn-sm";
    }
    return classes;
    }

}
