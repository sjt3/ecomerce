import { Component, OnInit } from '@angular/core';
import { Product } from 'src/app/interfaces';
import { PRODUCT } from 'src/app/data/products';
import { AddcartService } from 'src/app/services/addcart.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})


export class CartComponent implements OnInit {
  //private productsCartIds: number[] = []
  private idstring?: string;
  public productsInCart: Product[] | undefined;
  private products: Product[] | undefined;
  //private productsCant: [id: number, cant: number] = [0,0];


  constructor(private addCartService: AddcartService){

  }

  ngOnInit(): void {
    //throw new Error('Method not implemented.');
    let content = localStorage.getItem("idSelect");
    this.idstring = String(content);
    this.products = PRODUCT;
    this.getProductsInCart();
  }

  getCartIds(){
    return this.idstring;
  }

  getProductsInCart(){
    this.getCartIds();
    this.productsInCart = [];
    let idsCartSelect = this.idstring?.split(',');
    console.log(idsCartSelect);
    idsCartSelect?.forEach(idSelectVal => {
      let itemProdSel = this.products?.find(i => i.id === Number(idSelectVal));
      if (!itemProdSel) throw Error ('Item no encontrado');
      if (this.productsInCart?.find(e => e.id === Number(idSelectVal)) === undefined){
        this.productsInCart?.push(itemProdSel!);
        var idSel: number = Number(idSelectVal);
        //this.productsCant.push(idSel,1);
      } 
     this.cantidaProducto(Number(idSelectVal));  
    });
    
    return;
  }


  cantidaProducto(idProd: Number){
    let content = localStorage.getItem("idSelect");
    this.idstring = String(content);
    let idsCartSelect = this.idstring?.split(',');
    let arrCuenta = idsCartSelect?.filter(e => Number(e) === idProd);
    //console.log("LA CANTIDAD DE PRODUCTOS ES: "+arrCuenta?.length);
    return arrCuenta?.length;
  }

  calcularTotal(){
    this.getProductsInCart();
    var total: number = 0 ;
    var subtotal: number = 0 ;
    this.productsInCart?.forEach(objProd => {
      let cant = this.cantidaProducto(objProd.id);
      subtotal = cant * objProd.price;
      total = total + subtotal;
      subtotal = 0;
    });
    console.log("Calculando el total")
    return total;
  }


 /**llamado a servicios de carrito */
 addCartInList(idSelect: number){
  this.addCartService.addCart(idSelect);
  this.getProductsInCart();
  return;
}

elimineProdFromListCart(idSelect: number){
  this.addCartService.elimineProdFromListCart(idSelect);
  this.getProductsInCart();
  return;
}

productInCart(id: number){
  return this.addCartService.isIdInCart(id);
}

classAddingToCart(id: number){
  let valor = this.productInCart(id);
  let classes = "";
  if (valor){
    classes = "btn btn-secondary disabled product-btn btn-sm";
  } else {
    classes = "btn btn-primary product-btn btn-sm";
  }
  return classes;
  }
/**fin de llamado a servicos de carrito */
   

}
