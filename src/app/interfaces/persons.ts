
export interface Person {
    id: number;
    name:    string;
    lastName: string;
}

export interface Role {
    id: number;
    name:    string;
}

export interface User {
    id: number;
    personId: number;
    roleId: number;
}
