
export interface Category {
    id: number;
    name:    string;
    description: string;
}

export interface Product {
    id: number;
    title:    string;
    categoryId: number;
    mark:    string;
    model:    string;
    description: string;
    barcode:  string;
    mainImage: string;
    price: number;
}