import { User } from "./persons";
import { Product } from "./products";

export interface Sale{
    id: number;
    user: User;
    product: Product;
}