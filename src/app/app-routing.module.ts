import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProductListComponent } from './components/product-list/product-list.component';
import { LandingComponent } from './components/landing/landing.component';
import { ProductDetailComponent } from './components/product-detail/product-detail.component';
import { ProductImagesComponent } from './components/product-images/product-images.component';
import { ProductOpinionsComponent } from './components/product-opinions/product-opinions.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { UserListComponent } from './components/users/user-list/user-list.component';
import { CartComponent } from './components/cart/cart.component';

const routes: Routes = [
  {path: 'home', component: LandingComponent},
  {path: 'products', component: ProductListComponent},
  { path: 'products/:id', component: ProductDetailComponent,
      children: [
        {path: 'images', component: ProductImagesComponent},
        {path: 'opinions', component: ProductOpinionsComponent},
        {path: '', redirectTo: 'images', pathMatch: 'full'},
        {path: '**', component: NotFoundComponent}
      ]},
  { path: 'cart', component: CartComponent},
  { path: 'users', component: UserListComponent},
  { path: '', redirectTo: '/home', pathMatch: 'full'},
  { path: '**', component: NotFoundComponent}



];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
