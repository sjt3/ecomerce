import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AddcartService {

  private productsCartIds: number[] = []

  constructor() { }

  addCart(idSelect: number){
    this.productsCartIds = [];
    this.getLocalSorage();
    this.productsCartIds.push(idSelect);
    localStorage.removeItem('idSelect');
    this.grabarLocalSorage(this.productsCartIds.toString());
    return console.log("Se llamo al servicio add Cart");
  }

  grabarLocalSorage(id: string){
    localStorage.setItem("idSelect", id);
  }

  getLocalSorage(){
    let cartStringData = localStorage.getItem("idSelect");
    let cartArrData = cartStringData?.split(',');
    cartArrData?.forEach(element => {
      this.productsCartIds.push(Number(element));
    });
    return
  }

  isIdInCart(idSearch: number): boolean{
    this.getLocalSorage();
    //this.productsCartIds.find(value => {value == idSearch}, true);
    return this.productsCartIds.includes(idSearch);
  }

  elimineProdFromListCart(idSelect: number){
    this.productsCartIds = [];
    this.getLocalSorage();
    //eliminar todas las ocurrencias de idSelect desde el arreglo
    this.productsCartIds = this.elimineIdsOfArray(idSelect);
    localStorage.removeItem('idSelect');
    if (this.productsCartIds.length > 0){
        this.grabarLocalSorage(this.productsCartIds.toString());
      }
    return;
  }

  elimineIdsOfArray(idElim: number): number[]{
    console.log(this.productsCartIds);
    var arrTemp: number[] = [];
    this.productsCartIds.forEach(element => {
      if (element != idElim){
        arrTemp.push(element);
      }
    });
    console.log("llamado el eliminar");
    console.log(arrTemp);
    return arrTemp;
  }
 


}
